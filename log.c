//
// Created by dom on 25.02.2020.
//

#include "log.h"

void open_log_file(const char* file_name){
    log_file = fopen(file_name,"wt");
}

void close_log_file(){
    fclose(log_file);
}