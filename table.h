//
// Created by dom on 24.02.2020.
//

#ifndef TABLE_LAB_TABLE_H
#define TABLE_LAB_TABLE_H

#include "main.h"

typedef struct Table{
    Data* data_array;
    int size;
    int is_sort;
}Table;

Table* create_table();
void add_to_table(Table* table,Data data);

void sort_table(Table* table,int(*cmp)(Data* data1,Data* data2));

void save_table(Table* table, char* file_name);

Data* search_table(Table* table, int data,int mode);
void delete_from_table(Table* table,int index);
void open_table(Table* table, char* file_name);

#endif //TABLE_LAB_TABLE_H
