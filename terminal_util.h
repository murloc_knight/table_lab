//
// Created by dom on 24.02.2020.
//

#ifndef TABLE_LAB_TERMINAL_UTIL_H
#define TABLE_LAB_TERMINAL_UTIL_H

#ifdef WIN32
#include <windows.h>
#include "conio.h"
#define clear() system("cls")

#define F10 68
#define F1 59
#define F2 60
#define F3 61
#define F4 62
#define F5 63
#define F6 64
#define SPACE 32
#define RIGHT_ARROW 77
#define LEFT_ARROW 75
#define UP_ARROW 72
#define DOWN_ARROW 80
#define ESC 27
#define ENTER 13
#define BACK_SPACE 8

#define BLACK 0
#define BLUE 1
#define GREEN 2
#define CYAN 3
#define RED 4
#define MAGENTA 5
#define YELLOW 6
#define WHILE 7

#endif

#ifdef UNIX

//#define clear() printf("\033[H\033[J")
//#define getch() getchar()

#include <ncurses.h>

#define F10 274
#define F1 265
#define F2 266
#define F3 267
#define F4 268
#define F5 269
#define F6 270
#define SPACE 32
#define RIGHT_ARROW 261
#define LEFT_ARROW 260
#define UP_ARROW 259
#define DOWN_ARROW 258
#define ESC 27
#define ENTER 10
#define BACK_SPACE 127

#define BLACK 0
#define RED 1
#define GREEN 2
#define YELLOW 3
#define BLUE 4
#define MAGENTA 5
#define CYAN 6
#define WHILE 7

#define printf printw

#endif


void terminal_init();
void terminal_clear();
void set_cursor(int x, int y);
//void screen_size(int* x,int* y);
void set_color(int front,int back);
void reset_color();
void move_cursor(int x,int y);
int get_scren_x();
int get_scren_y();
void clear_row(int row);
int get_cursor_x();
int get_cursor_y();

#endif //TABLE_LAB_TERMINAL_UTIL_H
