//
// Created by dom on 11.03.2020.
//

#ifndef TABLE_LAB_UTIL_H
#define TABLE_LAB_UTIL_H
void time_to_hms(int time, short* h,short* m, short* s);
void date_to_dmy(int date, short* d,short* m,short* y);
#endif //TABLE_LAB_UTIL_H
