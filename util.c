//
// Created by dom on 11.03.2020.
//

#include "util.h"

inline void time_to_hms(int time, short* h,short* m, short* s){
    *h = (short) (time / 3600);
    *m = (short) ((time % 3600) / 60);
    *s = (short) ((time % 3600) % 60);
}

inline void date_to_dmy(int date, short* d,short* m,short* y){
    *y = (short) (date / 365);
    *m = (short) ((date%360) / 12);
    *d = (short) ((date%360) % 12);
}