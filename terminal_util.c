//
// Created by dom on 24.02.2020.
//

#include <stdio.h>
#include <stdlib.h>
#include "terminal_util.h"
#include <unistd.h>
#include <string.h>
#ifdef UNIX

int colornum(int fg, int bg)
{
    int B, bbb, ffff;

    B = 1 << 7;
    bbb = (7 & bg) << 4;
    ffff = 7 & fg;

    return (B | bbb | ffff);
}

#endif

int position[2];
int window_size[2];
char* space_row;

void terminal_init(){
    #ifdef WIN32
    CONSOLE_SCREEN_BUFFER_INFO info;

    GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE),&info);
    window_size[0] = info.srWindow.Right - info.srWindow.Left + 1;
    window_size[1] = info.srWindow.Bottom - info.srWindow.Top + 1;
#endif
#ifdef UNIX
	initscr();
	start_color();
	noecho();
	keypad(stdscr,1);
	getmaxyx(stdscr,window_size[1],window_size[0]);

	int fg, bg;
	int colorpair;
	
	for (bg = 0; bg <= 7; bg++) {
		for (fg = 0; fg <= 7; fg++) {
			colorpair = colornum(fg, bg);
			init_pair(colorpair, 7&fg, 7&bg);
	    }
	}
#endif
    space_row = malloc((size_t) window_size[0]-1);
    memset(space_row, ' ', (size_t) window_size[0]);
    space_row[window_size[0]-1] = 0;
}

void terminal_clear(){
	free(space_row);
#ifdef UNIX
	endwin();
#endif	
}

void set_cursor(int x, int y){
#ifdef WIN32
    COORD coord = {(short) x,(short) y};
    SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE),coord);
#endif
#ifdef UNIX
    move(y,x);
#endif
    position[0] = x;
    position[1] = y;
}

void move_cursor(int x,int y){
    set_cursor(position[0] + x, position[1] + y);
}

//void screen_size(int* x,int* y){
//#ifdef WIN32
//    CONSOLE_SCREEN_BUFFER_INFO info;
//
//    GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE),&info);
//    *x = info.srWindow.Right - info.srWindow.Left + 1;
//    *y = info.srWindow.Bottom - info.srWindow.Top + 1;
//#endif
//#ifdef UNIX
//    struct winsize w;
//    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
//    *x = w.ws_row;
//    *y = w.ws_col;
//#endif
//}

void set_color(int front,int back){
#ifdef WIN32
    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), (WORD) (front + back * 16));
#endif
#ifdef UNIX
	attron(COLOR_PAIR(colornum(7&front,back)));
#endif
}

void reset_color(){
#ifdef WIN32
    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 7);
#endif
#ifdef UNIX
    set_color(WHILE,BLACK);
#endif
}

void clear_row(int row){
    set_cursor(0,row);
    printf("%s",space_row);
}

inline int get_scren_x(){
    return window_size[0];
}

inline int get_scren_y(){
    return window_size[1];
}

inline int get_cursor_x(){
    return position[0];
}

inline int get_cursor_y(){
    return position[1];
}
