//
// Created by dom on 25.02.2020.
//

#ifndef TABLE_LAB_LOG_H
#define TABLE_LAB_LOG_H

#include <stdio.h>

FILE* log_file;

void open_log_file(const char* file_name);
void close_log_file();

#endif //TABLE_LAB_LOG_H
