#include <stdio.h>
#include <stdlib.h>
#include "terminal_util.h"
#include "main.h"
#include "table.h"
#include "log.h"
#include <math.h>
#include <string.h>
#include "util.h"
#define COLOMS 4
#define HELPS 7

void draw_table(Table* table,int begin,int count);
int draw_edit_window(Data *data);
void print_row(Data* data);
void draw_save_field();
void search_field(Table* table);
void draw_open_field(Table* table);

char* colom_titles[] = {
        "phone number",
        "date",
        "lenght",
        "city code"
};

char* help_titles[] = {
        "create F1",
        "delete F2",
        "search F3",
        "edit F4",
        "save F5",
        "open F6",
        "exit F10"
};

int current_row = 0;
int current_colom = 0;

int colom_size;
int scroll_table = 0;

int cmp_by_phone_nymber(Data* data1,Data* data2){
    fprintf(log_file,"%d %d\n",data1->phone_number,data2->phone_number);
    return data1->phone_number - data2->phone_number;
}

int cmp_by_date(Data* data1,Data* data2){
    for(int i = 3; i >= 0; i--){
        if (data1->date[i] != data2->date[i]){
            return data1->date[i] - data2->date[i];
        }
    }
}

int cmp_by_lenght(Data* data1,Data* data2){
    return data1->lenght - data2->lenght;
}

int cmp_by_city_code(Data* data1,Data* data2){
    return data1->city_code - data2->city_code;
}

int(*cmps[4])(Data* data1,Data* data2) = {
        cmp_by_phone_nymber,
        cmp_by_date,
        cmp_by_lenght,
        cmp_by_city_code
};

int main() {
    open_log_file("1.log");
	clear();
	terminal_init();
    colom_size = (get_scren_x())/COLOMS;
    draw_top_panel();
    draw_bottom_panel();
	
    Table* table = create_table();
	
    srand(24);
    Data data;
    for (int i = 0; i < 20; ++i) {

        data.phone_number = 20 - i;
        data.date[0] = (unsigned short) (rand() % 31);
        data.date[1] = (unsigned short) (rand() % 12);
        data.date[2] = (unsigned short) (rand()%9999);
        data.lenght = rand()%100000;
        data.city_code = i;
        add_to_table(table,data);
    }

    draw_table(table,scroll_table,get_scren_y()-3);
    int input = -1;
    do{
    	//printf("%d",input);
        fprintf(log_file,"%d\n",input);
        //right arrow key
        if (input == RIGHT_ARROW){
            if(current_colom < COLOMS-1){
                current_colom++;
                draw_top_panel();
            }
        }
        //left arrow key
        if (input == LEFT_ARROW){
            if(current_colom > 0){
                current_colom--;
                draw_top_panel();
            }
        }

        //down arrow key
        if (input == DOWN_ARROW){
            if (current_row < table->size-2){
                clear_row(current_row-scroll_table+1);
                set_cursor(0,current_row-scroll_table+1);
                print_row(&table->data_array[current_row]);
                if(current_row-scroll_table >= get_scren_y()-5){
                    scroll_table++;
                    draw_table(table,scroll_table,get_scren_y()-3);
                }
                current_row++;
                set_color(WHILE,CYAN);
                clear_row(current_row-scroll_table+1);
                set_cursor(0,current_row-scroll_table+1);
                print_row(&table->data_array[current_row]);
                set_color(WHILE,BLACK);
            }
        }
        //down up key
        if (input == UP_ARROW){
            if (current_row > 0){
                clear_row(current_row-scroll_table+1);
                set_cursor(0,current_row-scroll_table+1);
                print_row(&table->data_array[current_row]);
                if(current_row-scroll_table == 0){
                    scroll_table--;
                    draw_table(table,scroll_table,get_scren_y()-3);
                }
                current_row--;
                set_color(WHILE,CYAN);
                clear_row(current_row-scroll_table+1);
                set_cursor(0,current_row-scroll_table+1);
                print_row(&table->data_array[current_row]);
                set_color(WHILE,BLACK);
            }
        }
        //F1 key
        if (input == F1){
        	Data data = {0};
            if(draw_edit_window(&data)){
                add_to_table(table,data);
            }
            clear();
            draw_top_panel();
            draw_bottom_panel();
            draw_table(table,scroll_table,get_scren_y()-3);
        }
        //space key
        if (input == SPACE){
            sort_table(table,cmps[current_colom]);
            draw_table(table,scroll_table,get_scren_y()-3);
        }
        //F4 key
        if (input == F5){
        	draw_save_field(table);
        }

        if (input == F3){
            search_field(table);
        }

        if (input == F2 && table->size > 1){
            delete_from_table(table,current_row);
            if(current_row == table->size-1){
            	current_row--;
            }
            clear();
            draw_top_panel();
            draw_bottom_panel();
            draw_table(table,scroll_table,get_scren_y()-3);
        }

        if (input == F4){
            draw_edit_window(&table->data_array[current_row]);
            clear();
            draw_top_panel();
            draw_bottom_panel();
            draw_table(table,scroll_table,get_scren_y()-3);
        }

        if (input == F6){
            draw_open_field(table);
            draw_top_panel();
            draw_bottom_panel();
            draw_table(table,scroll_table,get_scren_y()-3);
        }

#ifdef UNIX
		refresh();
#endif
        
    }while ((input = getch()) != F10);
    reset_color();
    terminal_clear();
    close_log_file();
    clear();
    return 0;
}

void draw_bottom_panel(){
    set_color(BLACK,CYAN);
    int help_size = (get_scren_x())/HELPS;
    for (int i = 0,j = 0; j < HELPS; j++,i+=help_size) {
    	set_cursor((short) i, (short) (get_scren_y()-2));

        printf("%s",help_titles[j]);
    }
    reset_color();
}

void draw_top_panel(){
	set_color(BLACK,GREEN);
	clear_row(0);
    for (int i = 0,j = 0; j < COLOMS;i+= colom_size,j++) {
        if (j == current_colom){
            set_color(BLACK,CYAN);
        }

        set_cursor((short) i, 0);

        printf("%s",colom_titles[j]);
        set_color(BLACK,GREEN);
    }
    reset_color();
}

void print_row(Data* data){

    printf("%u",data->phone_number);

    move_cursor(colom_size,0);
    printf("%02u.%02u.%04u",data->date[0],data->date[1],data->date[2]);

    move_cursor(colom_size,0);

    short h,m,s;
    time_to_hms(data->lenght,&h,&m,&s);

    printf("%u:%u:%u",h,m,s);

    move_cursor(colom_size,0);
    printf("%u",data->city_code);
}
//TODO сортировка в обратном порядке
void draw_table(Table* table,int begin,int count){
    int current = begin;

    for (int i = 1; i < count && current < table->size-1 ; ++i,current++) {
    	if(current == current_row){
    		set_color(WHILE,CYAN);
    	}else{
    		reset_color();
    	}
        clear_row(i);
        set_cursor(0,i);
        print_row(&table->data_array[current]);
    }
}
//TODO движение курсора по тексту сохранения
void draw_save_field(Table* table){
	int input;
	set_color(BLACK,CYAN);
    clear_row(get_scren_y()-3);
    set_cursor(0,get_scren_y()-3);
    char file_name[20];
    int input_x = 0;
    while((input = getch()) != ESC && input != F10){
		
		if(input == BACK_SPACE){
        	printf(" ");
            file_name[input_x--] = 0;
            if(input_x >= 0)
            	set_cursor(input_x,get_scren_y()-3);
            else
            	set_cursor(++input_x,get_scren_y()-3);
            continue;
        }

		if (input == ENTER){
            save_table(table,file_name);

            break;
		}

        if(((input >= 'a' && input <= 'z') || input == ' ' || input == '.') && input_x < 20){
            printf("%c",input);
            file_name[input_x++] = (char) input;
        }
    }

    reset_color();
   	clear_row(get_scren_y()-3);	
}

void search_field(Table* table){
    int input;
    set_color(BLACK,CYAN);
    clear_row(get_scren_y()-3);
    set_cursor(0,get_scren_y()-3);
    int search_data = 0;
    int input_x = 0;
    while((input = getch()) != ESC && input != F10){

        if(input == BACK_SPACE){
            printf(" ");
            search_data /= 10;
            input_x--;
            if(input_x >= 0)
                set_cursor(input_x,get_scren_y()-3);
            else
                set_cursor(++input_x,get_scren_y()-3);
            continue;
        }

        if (input == ENTER){
            Data* result = search_table(table,search_data,current_colom);
            draw_edit_window(result);
            clear();
            draw_top_panel();
            draw_bottom_panel();
            draw_table(table,scroll_table,get_scren_y()-3);
            break;
        }

        if((input >= '0' && input <= '9') && input_x < 6){
            printf("%c",input);
            search_data *= 10;
            search_data += (input-'0');
        }
    }

    reset_color();
    clear_row(get_scren_y()-3);
}

void draw_open_field(Table* table){
    int input;
    set_color(BLACK,CYAN);
    clear_row(get_scren_y()-3);
    set_cursor(0,get_scren_y()-3);
    char file_name[20];
    int input_x = 0;
    while((input = getch()) != ESC && input != F10){

        if(input == BACK_SPACE){
            printf(" ");
            file_name[input_x--] = 0;
            if(input_x >= 0)
                set_cursor(input_x,get_scren_y()-3);
            else
                set_cursor(++input_x,get_scren_y()-3);
            continue;
        }

        if (input == ENTER){
            open_table(table,file_name);

            break;
        }

        if(((input >= 'a' && input <= 'z') || input == ' ' || input == '.') && input_x < 20){
            printf("%c",input);
            file_name[input_x++] = (char) input;
        }
    }

    reset_color();
    clear_row(get_scren_y()-3);
}

int draw_edit_window(Data *data){
    const int fielt_size[4] = {
            6,8,6,3
    };

    char* fielt_data[4] = {
            malloc(7),
            malloc(9),
            malloc(7),
            malloc(4)
    };
    fielt_data[0][6] = 0;
    fielt_data[1][8] = 0;
    fielt_data[2][6] = 0;
    fielt_data[3][3] = 0;
    int h = data->lenght/3600;
    int m = (data->lenght%3600)/60;
    int s = (data->lenght%3600)%60;

    sprintf(fielt_data[0],"%06d",data->phone_number);
    sprintf(fielt_data[1],"%02d%02d%04d",
           data->date[0],
           data->date[1],
           data->date[2]
    );
    sprintf(fielt_data[2],"%02d%02d%02d",h,m,s);
    sprintf(fielt_data[3],"%03d",data->city_code);
    int field_x = 0;
    
    set_color(WHILE,WHILE);
    int x = get_scren_x()/2-20;
    int y = get_scren_y()/2-10;

    int fider_id = 0;

    for (int i = y; i < y+15; ++i) {
        set_cursor(x,i);
        printf("                                        ");
    }

    set_cursor(x+1,y+1);
    set_color(BLACK,WHILE);
    printf("phone number ");
    set_color(WHILE,BLUE);
    printf("%06d",data->phone_number);
    set_color(BLACK,WHILE);

    set_cursor(x+1,y+3);
    printf("date ");
    set_color(WHILE,BLUE);
    printf("%02d%02d%04d",
    	data->date[0],
    	data->date[1],
    	data->date[2]
    );
    set_color(BLACK,WHILE);



    set_cursor(x+1,y+5);
    printf("lenght ");
    set_color(WHILE,BLUE);
    printf("%02d%02d%02d",h,m,s);
    set_color(BLACK,WHILE);

    set_cursor(x+1,y+7);
    printf("city code ");
    set_color(WHILE,BLUE);
    printf("%03d",data->city_code);
    set_color(BLACK,WHILE);;
    set_cursor(x+14,y+1);
	int input = 0;
    while((input = getch()) != ESC && input != F1 &&  input != F10){
        if (input == DOWN_ARROW && fider_id < 3){
            fider_id++;
            set_cursor((int) (x + strlen(colom_titles[fider_id]) + 2),y+ fider_id * 2 + 1);
            field_x = 0;
            continue;
        }

        if (input == UP_ARROW && fider_id > 0){
            fider_id--;
            set_cursor((int) (x + strlen(colom_titles[fider_id]) + 2),y+ fider_id * 2 + 1);
            field_x = 0;
            continue;
        }

        if (input == RIGHT_ARROW && field_x < fielt_size[fider_id]-1){
            move_cursor(1,0);
            field_x++;
            continue;
        }

        if (input == LEFT_ARROW && field_x > 0){
            move_cursor(-1,0);
            field_x--;
            continue;
        }
        if(input >= '0' && input <= '9'){
            set_color(WHILE,BLUE);
            printf("%c",input);
            fielt_data[fider_id][field_x] = (char) input;
            if (field_x < fielt_size[fider_id]-1) {
                move_cursor(1, 0);
                field_x++;
            }
            else {
                move_cursor(0, 0);
            }
        }
        if (input == BACK_SPACE){
            set_color(WHILE,BLUE);
            printf(" ");
            fielt_data[fider_id][field_x] = ' ';
            if (field_x > 0) {
                move_cursor(-1, 0);
                field_x--;
            }
            else{
                move_cursor(0, 0);
            }
        }
        //enter key
        if (input == ENTER){
            int phone_number = strtol(fielt_data[0],0,10);
            long data_raw = strtol(fielt_data[1],0,10);
            unsigned short date[3];
            date[2] = (unsigned short) (data_raw % 10000);
            data_raw/= 10000;
            date[1] = (unsigned short) (short) (data_raw % 100);
            data_raw /= 100;
            date[0] = (unsigned short) (short) data_raw;

            if (date[0] > 31 || date[1] > 12 || (date[0] == 0 && date[1] == 0 && date[2] == 0)){
                int old_curlor[2];
                old_curlor[0] = get_cursor_x();
                old_curlor[1] = get_cursor_y();

                set_cursor(x,y);
                set_color(WHILE,RED);
                printf("incorect date format");
                set_color(WHILE,BLUE);
                set_cursor(old_curlor[0],old_curlor[1]);
                continue;
            }

            long lenght_raw = strtol(fielt_data[2],0, 10);
            int lenght[3];
            lenght[2] = lenght_raw%100;
            lenght_raw/= 100;
            lenght[1] = lenght_raw%100;
            lenght_raw /= 100;
            lenght[0] = lenght_raw;

            if (lenght[2] > 59 || lenght[1] > 59 || (lenght[0] == 0 && lenght[1] == 0 && lenght[2] == 0)){
                int old_curlor[2];
                old_curlor[0] = get_cursor_x();
                old_curlor[1] = get_cursor_y();

                set_cursor(x,y);
                set_color(WHILE,RED);
                printf("incorect lenght format");
                set_color(WHILE,BLUE);
                set_cursor(old_curlor[0],old_curlor[1]);
                continue;
            }

            int city_code = strtol(fielt_data[3],0,10);
            fprintf(log_file,"%d [%d:%d:%d] [%d:%d:%d] %d\n",
                    phone_number,
                    date[0],
                    date[1],
                    date[2],
                    lenght[0],
                    lenght[1],
                    lenght[2],
                    city_code
            );
            
            data->phone_number = phone_number;
            data->date[0] = date[0];
            data->date[1] = date[1];
            data->date[2] = date[2];
            data->lenght = lenght[2] + lenght[1] * 60 + lenght[0] * 3600;
            data->city_code = city_code;

            free(fielt_data[0]);
            free(fielt_data[1]);
            free(fielt_data[2]);
            free(fielt_data[3]);
            reset_color();
            return 1;
        }
    }
    free(fielt_data[0]);
    free(fielt_data[1]);
    free(fielt_data[2]);
    free(fielt_data[3]);
    reset_color();
    return 0;
}