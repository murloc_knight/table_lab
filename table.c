//
// Created by dom on 24.02.2020.
//

#include "table.h"
#include <malloc.h>
#include <stdlib.h>
#include <stdio.h>
#include "util.h"
#include "log.h"

Table* create_table(){
    Table* t = malloc(sizeof(Table));
    t->data_array = malloc(sizeof(Data));
    t->size = 1;
    t->is_sort = 0;
    return t;
}

void add_to_table(Table* table,Data data){
    table->size++;
    table->data_array = realloc(table->data_array,table->size * sizeof(Data));
    table->data_array[table->size-2] = data;
}

void sort_table(Table* table,int(*cmp)(Data* data1,Data* data2)){
    qsort(table->data_array, (size_t) table->size-1, sizeof(Data), (int (*)(const void *, const void *)) cmp);
}

void save_table(Table* table, char* file_name){
	FILE* file = fopen(file_name,"w");
	fprintf(file,"%d\n",table->size-1);
	for(int i =0; i < table->size-1;i++){
		Data* data = &table->data_array[i];
        short h,m,s;
        time_to_hms(data->lenght,&h,&m,&s);
	
		fprintf(file,"%d [%d %d %d] [%d %d %d] %d\n",
			data->phone_number,
			data->date[0],
			data->date[1],
			data->date[2],
			h,m,s,
			data->city_code
		);
	}
	fclose(file);
}

Data* search_table(Table* table, int data,int mode){
    Data* result = &table->data_array[0];
    for (int i = 1; i < table->size - 1; ++i) {
        switch (mode){
            case 0:
                if (abs(result->phone_number - table->data_array[i].phone_number) > abs(data - table->data_array[i].phone_number)){
                    result = &table->data_array[i];
                }
                break;
            case 1: {
                int data_raw = data;
                unsigned short date[3];
                date[2] = (unsigned short) (data_raw % 10000);
                data_raw/= 10000;
                date[1] = (unsigned short) (short) (data_raw % 100);
                data_raw /= 100;
                date[0] = (unsigned short) (short) data_raw;

                for (int j = 2; j >= 0;j--){
                    if (abs(date[j] - result->date[j]) > abs(date[j] - table->data_array[i].date[j])){
                        result = &table->data_array[i];
                    }
                }
                break;
                case 2: {
                    long lenght_raw = data;
                    int lenght;
                    lenght = (lenght_raw%100);
                    lenght_raw/= 100;
                    lenght += (lenght_raw%100) * 60;
                    lenght_raw /= 100;
                    lenght += lenght_raw * 3600;
                    fprintf(log_file,"T %d\n",lenght);
                    if (abs(result->lenght - lenght) > abs(table->data_array[i].lenght - lenght)) {
                        result = &table->data_array[i];
                    }
                    }
                    break;
                case 3:
                    if (abs(result->city_code - table->data_array[i].city_code) > abs(data - table->data_array[i].city_code)){
                        result = &table->data_array[i];
                    }
                    break;
            }
        }
    }
    return result;
}

void delete_from_table(Table* table,int index){
    for (int i = index; i < table->size - 2; ++i) {
        table->data_array[i] = table->data_array[i+1];
    }
    table->data_array = realloc(table->data_array, (size_t) (--table->size) * sizeof(Data));
}

void open_table(Table* table, char* file_name){
    FILE* file = fopen(file_name,"r");
    int size;
    fscanf(file,"%d\n",&size);

    free(table->data_array);
    table->size = size+1;
    table->data_array = malloc((size+1) * sizeof(Data));
    Data data;
    int h,m,s;
    for (int i = 0; i < size; ++i) {
        fscanf(file,"%d [%hu %hu %hu] [%d %d %d] %d\n",
                &data.phone_number,
                &data.date[0],&data.date[1],&data.date[2],
                &h,&m,&s,
                &data.city_code
        );
        data.lenght = s + m*60 + h*3600;
        table->data_array[i] = data;
    }
}